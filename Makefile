SOURCEDIR = src
BUILDDIR = obj

CC=gcc
_CFLAGS=$(CFLAGS) -I$(SOURCEDIR)  -ggdb -Werror -std=gnu11

CC=gcc

ODIR=obj
LDIR =../lib

LIBS=
_DEPS = 
_OBJ =

SOURCES := $(shell find $(SOURCEDIR) -name '*.c'  -not -ipath '*vm_tester*')
HEADERS := $(shell find $(SOURCEDIR) -name '*.h'  -not -ipath '*vm_tester*')

TESTER_SOURCES := $(shell find $(SOURCEDIR) -name '*.c'  -not -name 'vm.c')
TESTER_HEADERS := $(shell find $(SOURCEDIR) -name '*.h'  -not -name 'vm.h')

OBJECTS := $(addprefix $(BUILDDIR)/,$(SOURCES:%.c=%.o))
DEP = $(OBJECTS:%.o=%.d)

TESTER_OBJECTS := $(addprefix $(BUILDDIR)/,$(TESTER_SOURCES:%.c=%.o))
TESTER_DEP = $(TESTER_OBJECTS:%.o=%.d)

all: vm tester tags

gen_opcodes: $(SOURCEDIR)/opcodes/gen_opcodes.py
	cd $(SOURCEDIR)/opcodes; python3 gen_opcodes.py


-include $(DEP) $(TESTER_DEP)

$(BUILDDIR)/%.o: %.c
	mkdir -p "`dirname $@`"
	$(CC) -MMD -c -o $@ $< $(_CFLAGS)

tags: $(SOURCES) $(HEADERS)
	ctags -f TAGS --recurse  --c-kinds=+p --fields=+S src

vm: $(OBJECTS) $(HEADERS)
	$(CC) -fPIC -Wl,-Bdynamic -o vm $(OBJECTS) $(_CFLAGS) $(LIBS)

tester: $(TESTER_OBJECTS) $(TESTER_HEADERS)
	$(CC) -fPIC -Wl,-Bdynamic -o tester $(TESTER_OBJECTS) $(_CFLAGS) $(LIBS)
	


.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ $(shell find $(ODIR) -iname *.o -or -iname *.d)

