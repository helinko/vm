#include "cpu.h"
#include "common.h"
#include "opcodes/opcodes.h"
#include "colors.h"

#include <signal.h>
#include <stdio.h>
#include <string.h>

static uint8_t trace(cpu* cpu, const uint8_t* code)
{
    static char disassembly[128];
    memset(disassembly, 0, 128);
    uint8_t processed = (*disassemble_op[*GET_IP_ADDR(cpu)])
        (cpu,
        disassembly,
        127);

    if (processed > 0)
    {
        printf("\t[F:%d]\t\t\t%04X - %s\n", cpu->flags, cpu->ip, disassembly);
    }
    else
    {
        printf("\t\t\t\t%04X - disassembly failed, retry at next ip.\n"
        "OPCODE [%d]",
        cpu->ip,
        *GET_IP_ADDR(cpu));
    }

    return processed;
}

static void print_stack(cpu* cpu, uint8_t* orig_bp)
{
    for(uint8_t *pos = orig_bp; pos < GET_SP_ADDR(cpu); pos++)
    {
        if (pos == GET_BP_ADDR(cpu)-1)
        {
            printf("%s%02X%s, ", KRED, *pos, KNRM);
        }
        else if (pos == GET_BP_ADDR(cpu)-4)
        {
            printf("%s%02X%s, ", KGRN, *pos, KNRM);
        }
        else
        {
            printf("%02X, ", *pos);
        }
    }
}

// ** Extern ** //

extern inline void stack_push(cpu* cpu, uint8_t byte);
extern inline uint8_t stack_pop(cpu* cpu);
extern inline void stack_push_16(cpu* cpu, uint16_t val);
extern inline uint16_t stack_pop_16(cpu* cpu);
extern inline uint8_t fetch(cpu* cpu);
extern inline uint16_t fetch_16(cpu* cpu);
extern inline void flag_set(cpu* cpu, CPU_FLAGS flag, uint8_t val);
extern inline int flag_isset(cpu* cpu, CPU_FLAGS flag);
extern inline void control_flag_set(cpu* cpu, CPU_CONTROL_FLAGS flag, uint8_t val);
extern inline int control_flag_isset(cpu* cpu, CPU_CONTROL_FLAGS flag);

cpu *init_cpu(uint16_t stack_size)
{
    cpu *new =  init_cpu_with_stack(malloc(stack_size));
    control_flag_set(new, CONTROL_FLAG_USER_STACK, 0);

    return new;
}

cpu *init_cpu_with_stack(uint8_t* stack)
{
    cpu* new = malloc(sizeof(cpu));
    memset(new, 0, sizeof(cpu));
    new->data = stack;
    new->bp = new->sp = 0;
    new->control_flags |= CONTROL_FLAG_USER_STACK;

    return new;
}

void free_cpu(cpu* cpu)
{
    if (control_flag_isset(cpu, CONTROL_FLAG_USER_STACK))
    {
        free(cpu->data);
    }

    free(cpu);
}


void run(cpu* cpu, const uint8_t* code)
{
    flag_set(cpu, FLAG_RUN, 1);
    cpu->code = code;
    while (flag_isset(cpu, FLAG_RUN))
    {
#ifdef _STACK_TRACE
        printf("[BP:%d/%d]", cpu->bp, cpu->sp);
#endif
#ifdef _FULL_STACK_TRACE
        printf("\t[");
        print_stack(cpu, cpu->data);
        printf("]\n");
#endif
#ifdef _TRACE
        trace(cpu, code);
#endif

        uint8_t opcode = fetch(cpu);
        (*execute_op[opcode])(cpu);

    }
}

char* disassemble(cpu* cpu, const uint8_t* code, const uint16_t size)
{
    size_t buffer_size = 512;
    size_t pos = 0;
    char* buffer = realloc(NULL, buffer_size);
    char disassembly[128];
    char prefix[12];
    memset(disassembly, 0, sizeof(disassembly));
    cpu->code = code;

    while (cpu->ip < size)
    {
        memset(prefix, 0, sizeof(prefix));
        memset(disassembly, 0, sizeof(disassembly));
        snprintf(prefix, sizeof(prefix), "%04X - ", cpu->ip);
        uint8_t op = GET_IP_B_VAL(cpu, 0);
        uint8_t processed = (*disassemble_op[op])
            (cpu,
            disassembly,
            127);
        cpu->ip += processed;

        if (processed == 0)
        {
            break;
        }

        size_t prefix_len = strnlen(prefix, sizeof(prefix)-1);
        size_t disassembly_len = strnlen(disassembly, sizeof(disassembly)-1);
        if (buffer_size < (pos +
            disassembly_len +
            prefix_len +  4))
        {
            buffer_size *= 2;
            buffer = realloc(buffer, buffer_size);
        }

        strncpy(buffer+pos, prefix, prefix_len);
        pos += prefix_len;
        strncpy(buffer+pos, disassembly, disassembly_len);
        pos += disassembly_len;
        buffer[pos++] = '\n';
        buffer[pos] = '\0';
    }

    return buffer;
}

char* print_cpu(cpu* cpu)
{
    size_t size = 64;
    size_t pos = 0;
    char* buffer = malloc(size);
    pos  += snprintf(buffer, size-pos, "IP %04X\n", cpu->ip);
    pos  += snprintf(buffer+pos, size-pos, "SP %04X\n", cpu->sp);
    pos  += snprintf(buffer+pos, size-pos, "BP %04X \n", cpu->bp);
    pos  += snprintf(buffer+pos, size-pos, "FLAGS %" PRIu8 " \n", cpu->flags);
    return buffer;
}
