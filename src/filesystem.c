#include <stdio.h>
#include "filesystem.h"

char* read_file(const char* path)
{
    FILE* file = fopen(path, "rb");                

    if (file == NULL)
    {
        return NULL;
    }

    fseek(file, 0L, SEEK_END);
    size_t size = ftell(file);
    rewind(file);

    char* result = malloc(size+1);
    if (result == NULL)
    {
        fclose(file);
        return NULL;
    }
    size_t read = fread(result, sizeof(char), size, file);
    result[read] = '\0';

    fclose(file);
    return result;
}

