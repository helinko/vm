#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "common.h"
#include "assembler.h"
#include "opcodes/opcodes.h"
#include "ds/intrusive_list.h"

typedef enum
{
    TOKEN_OP_CODE,
    TOKEN_LABEL,
    TOKEN_LITERAL
} TOKEN_TYPE;

typedef struct token_t
{
    TOKEN_TYPE type;
    char* text;
} token;

typedef struct label_t
{
    intrusive_list_t list;
    const char* label;
    uint16_t pos;
} label_t;

static bool at_end(const char* source)
{
    return  *source == 0;
}

static bool is_whitespace(const char c)
{
    switch(c)
    {
        case ' ':
        case '\t':
        case '\n':
        case '\r':
            return true;
    }

    return false;
}

static const char* skip_whitespace(const char* source)
{
    const char* pos = source;

    while (is_whitespace(*pos) && !at_end(pos))
    {
        pos++;
    }

    return pos;
}

static char* next(const char* source, char* token, size_t *source_pos)
{
    const char* current = skip_whitespace(source + *source_pos);
    if (at_end(current))
    {
        *token = 0;
        return token;
    }

    int pos = 0;
    while (pos < MAX_TOKEN_LENGTH && !is_whitespace(*current) && !at_end(current))
    {
        token[pos++] = *current++;
    }
    *source_pos = current - source;

    token[pos] = '\0';

    return token;
}

static bool ends_with(const char* str, const char what)
{
    return str[strlen(str)-1] == what;
}

static int get_op_code_index(const char* str)
{
    for (int i = 0; i < NUM_OPCODES; i++)
    {
        if (strncasecmp(str, op_code_names[i], MAX_TOKEN_LENGTH) == 0)
        {
            return i;
        }
    }

    return -1;
}

static bool is_op_code(const char* str)
{
    return get_op_code_index(str) != -1;
}

static token make_token(const char* str)
{
    token retval;
    retval.text = NULL;
    if (ends_with(str, ':'))
    {
        retval.type = TOKEN_LABEL;
        retval.text = strndup(str, MAX_TOKEN_LENGTH);
    }
    else if(is_op_code(str))
    {
        retval.type = TOKEN_OP_CODE;
        retval.text = strndup(str, MAX_TOKEN_LENGTH);
    }
    else
    {
        retval.type = TOKEN_LITERAL;
        retval.text = strndup(str, MAX_TOKEN_LENGTH);
    }

    return retval;
}

static void free_token(token t)
{
    if(t.text != NULL)
    {
        free(t.text);
        t.text = NULL;
    }
}

static label_t new_label_list()
{
    label_t retval;
    memset(&retval, 0, sizeof(label_t));
    return retval;
}

static label_t* make_label(const char* name, uint16_t pos)
{
    label_t *retval = malloc(sizeof(label_t));
    // Drop the : from label name
    int strip = ends_with(name, ':') ? 1 : 0;
    retval->label = strndup(name, strnlen(name, MAX_TOKEN_LENGTH)-strip);
    retval->pos = pos;
    return retval;
}

static void free_label(label_t* label)
{
    if (label->label != NULL)
    {
        free((char*)label->label);
        label->label = NULL;
    }

    free(label);
}

static bool is_number(const char* text)
{
    while (*text != '\0')
    {
        if (!isdigit(*text) && *text != '+' && *text != '-')
        {
            return false;
        }
        text++;
    }

    return true;
}

static bool label_comparer(const intrusive_list_t* item, const void* needle)
{
    label_t* l = (label_t*)item;
    const char* name = needle;

    if (l->label != NULL && strncmp(l->label, name, MAX_TOKEN_LENGTH) == 0)
    {
        return true;
    }

    return false;
}

static bool is_label_ref(const char* text)
{
    return isalpha(*text) && *text != '\"';
}

static bool has_attribute(uint8_t opcode, OP_CODE_ATTRIBUTE attribute)
{
    attr_descriptor_t attr = op_code_attributes[opcode];
    for (int i = attr.attribute_count-1; i >=0; i--)
    {
        if (attr.attributes[i] == attribute)
        {
            return true;
        }
    }

    return false;
}

static void write_label_references(
    uint8_t* bytecode,
    label_t* refs_to_update,
    label_t* labels)
{
    while (refs_to_update != NULL)
    {
        label_t* target = (label_t*)list_find_item(
            &labels,
            label_comparer,
            refs_to_update->label);

        if (target != NULL)
        {
            uint16_t location = target->pos;
            uint8_t opcode = bytecode[refs_to_update->pos-1];
            if (has_attribute(opcode, ATTR_RELATIVE))
            {
                location = target->pos-refs_to_update->pos-1;
            }

            if (has_attribute(opcode, ATTR_WORD))
            {
                bytecode[refs_to_update->pos] = (uint8_t)location >> 8;
                bytecode[refs_to_update->pos+1] = (uint8_t)location & 0xFF;
            }
            else
            {
                bytecode[refs_to_update->pos] = (int8_t)(target->pos-refs_to_update->pos)-1;
            }
        }
        else
        {
            printf("Didn't find a label position for %s\n", refs_to_update->label);
        }

        refs_to_update = (label_t*)list_next((intrusive_list_t*)refs_to_update);
    }
}

uint8_t* assemble(const char* source, size_t* size)
{
    char token_str[MAX_TOKEN_LENGTH+1];
    uint8_t* result = malloc(sizeof(int8_t)*INITIAL_CODE_SEGMENT_SIZE);
    memset(result, 0 , sizeof(int8_t)*INITIAL_CODE_SEGMENT_SIZE);
    size_t bytecode_pos = 0;
    size_t source_pos = 0;
    label_t labels = new_label_list();
    label_t label_reqs = new_label_list();
    uint8_t last_bytecode = 255;

    while (*(next(source, token_str, &source_pos)) != '\0')
    {
        token cur = make_token(token_str);
        switch(cur.type)
        {
            case TOKEN_OP_CODE:
            {
                int index = get_op_code_index(cur.text);
                last_bytecode = (uint8_t)index;
                result[bytecode_pos++] = index;
                break;
            }
            case TOKEN_LITERAL:
            {
                if (is_number(cur.text) && has_attribute(last_bytecode, ATTR_BYTE))
                {
                    result[bytecode_pos++] = (int8_t)atoi(cur.text);
                }
                else if (is_number(cur.text) && has_attribute(last_bytecode, ATTR_WORD))
                {
                    int16_t number = (int16_t)atoi(cur.text);
                    result[bytecode_pos++] = number & 0xFF;
                    result[bytecode_pos++] = (number >> 8) & 0xFF;
                }
                else if (is_label_ref(cur.text))
                {
                    label_t* ref = make_label(cur.text, bytecode_pos);
                    list_append(&label_reqs, (intrusive_list_t*)ref);
                    if (has_attribute(result[bytecode_pos-1], ATTR_WORD))
                    {
                        result[bytecode_pos++] = 0;
                    }

                    result[bytecode_pos++] = 0;
                }
                else
                {
                    printf("Unknown literal type %s\n", token_str);
                    exit(-70);
                }
                break;
            }
            case TOKEN_LABEL:
            {
                label_t* current_label = make_label(cur.text, bytecode_pos);
                list_append(&labels, (intrusive_list_t*)current_label);
                break;
            }
        }
        free_token(cur);
    }

    write_label_references(result,
       (label_t*)list_next((intrusive_list_t*)&label_reqs),
       &labels);

    *size = bytecode_pos;

    label_t* cleanup = (label_t*)list_next((intrusive_list_t*)&label_reqs);
    while (cleanup != NULL)
    {
        label_t* next = (label_t*)list_next((intrusive_list_t*)cleanup);
        free_label(cleanup);
        cleanup = next;
    }

    cleanup = (label_t*)list_next((intrusive_list_t*)&labels);
    while (cleanup != NULL)
    {
        label_t* next = (label_t*)list_next((intrusive_list_t*)cleanup);
        free_label(cleanup);
        cleanup = next;
    }

    return result;
}
