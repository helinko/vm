#include <stdio.h>
#include "test.h"
#include "cpu.h"
#include "filesystem.h"
#include "ds/intrusive_list.h"
#include "colors.h"

static bool print_test(const intrusive_list_t* item, const void* criteria)
{
    test_t* test = (test_t*)item;
    test_result_t* result = test->last_result;
    int status = *(int*)criteria;
    if (result != NULL)
    {
        printf(KNRM "\tTest: %s\n", test->name);
        if (result->message != NULL)
        {
            printf("\t\t%s\n", result->message);
        }

        for (int i = 0; i < result->verifier_result_count; i++)
        {
            int verifier_status = result->verifier_results[i]->status;
            printf(verifier_status == false ? KRED : KGRN);
            printf("\t\tVerifier: %s", result->verifier_results[i]->info->name);
            if (verifier_status <= status &&
                result->verifier_results[i]->message != NULL)
            {
                printf(" %s", result->verifier_results[i]->message);
            }
            printf(KNRM "\n");
        }
    }
    else
    {
        printf(KYEL "Test not run: %s\n" KNRM, test->name);
    }

    return true;
}

static bool test_status(const intrusive_list_t* item, const void* criteria)
{
    test_t* test = (test_t*)item;
    test_result_t* result = test->last_result;
    int status = *(int*)criteria;
    if (result != NULL)
    {
        for (int i = 0; i < result->verifier_result_count; i++)
        {
            int verifier_status = result->verifier_results[i]->status;
            if (verifier_status == status)
            {
                return true;
            }
        }
    }

    return false;
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printf("Usage: %s [path to test file]\n", argv[0]);
        return -1;
    }

    int tests_run = 0;
    int tests_failed = 0;

    for (int i = 1; i < argc; i++)
    {
        char* test_src = read_file(argv[i]);
        if (test_src == NULL)
        {
            printf("Couldn't read %s\n", argv[i]);
            return -2;
        }

        printf("Running test from: %s\n", argv[i]);
        test_t* tests = create_tests(test_src);
        free(test_src);
        int total_tests = list_item_count(tests, NULL, NULL);
        tests_run += total_tests;
        int failures = 0;

        if (!run_tests(tests))
        {
            failures++;
        }
        int print_status = 2;
        list_item_count(tests, print_test, (void*)&print_status);
        int failure_status = 0;
        int failed_tests = list_item_count(tests, test_status, (void*)&failure_status);
        tests_failed += failed_tests;
        free_tests(tests);
        printf("========= Status %d/%d\n", (total_tests-failed_tests), total_tests);
    }

    printf("Test run summary: Total tests run %d, failures %d\n",
        tests_run, tests_failed);

    return tests_failed;
}
