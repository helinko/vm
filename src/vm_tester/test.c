#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <signal.h>
#include <setjmp.h>
#include "test.h"
#include "assembler.h"
#include "cpu.h"

#define MAX_LINE_LENGTH 256
// 170 == 10101010b
#define STACK_INITIALIZATION_PATTERN 170

#define PRINT_ALLOCATE_BUFFER(name, fmt, ...) do {\
    size_t __name_len = snprintf(NULL, 0, (fmt), __VA_ARGS__) + 1;\
    name = malloc(__name_len);\
    snprintf(name, __name_len, (fmt), __VA_ARGS__); \
    } while(0);

static bool at_end(const char* source)
{
    return *source == 0;
}

static bool is_whitespace(const char* c)
{
    switch(*c)
    {
        case ' ':
        case '\t':
        case '\n':
        case '\r':
            return true;
    }

    return false;
}

static bool is_line_end(const char* c)
{
    return *c == '\n';
}

static const char* skip_whitespace(const char* source)
{
    const char* pos = source;

    while (is_whitespace(pos) && !at_end(pos))
    {
        pos++;
    }

    return pos;
}

static bool info_name_comparer(const intrusive_list_t* item, const void* criteria)
{
    test_t* test = (test_t*)item;
    return strcmp(test->name, (const char*)criteria) == 0;
}

static bool has_verifier(const intrusive_list_t* item, const void* criteria)
{
    test_info_t* test = (test_info_t*)item;
    return test->verifier != NULL;
}

static bool verifier_status(const intrusive_list_t* item, const void* criteria)
{
    test_verifier_result_t* result = (test_verifier_result_t*)item;
    return result->status == *(int*)criteria;
}


static char* next(const char* buffer, char* line, size_t *buffer_pos)
{
    const char* current = skip_whitespace(buffer + *buffer_pos);
    if (at_end(current))
    {
        *line = 0;
        return line;
    }

    int pos = 0;
    while (pos < MAX_LINE_LENGTH && !is_line_end(current) && !at_end(current))
    {
        line[pos++] = *current++;
    }
    *buffer_pos = current - buffer;
    line[pos] = '\0';

    return line;
}

static bool is_test_start(const char* line)
{
    const char* data_start = skip_whitespace(line);
    return strncmp(data_start, "**", 2) == 0;
}

static const char* copy_test_name(const char* line)
{
    const char* name_start = skip_whitespace(line+2);
    return strndup(name_start, MAX_LINE_LENGTH-(name_start-line+1));
}

static bool is_test_info(const char* line)
{
    const char* skipped = skip_whitespace(line);
    return (line - skipped) < MAX_LINE_LENGTH-2 &&
        skipped[0] == '*' &&
        skipped[1] != '*';
}

static test_verifier_result_t* make_test_verifier_result(
    int status,
    const test_info_t* info,
    const char* message)
{
    test_verifier_result_t* result = malloc(sizeof(test_verifier_result_t));
    memset(result, 0, sizeof(test_verifier_result_t));
    result->status = status;
    result->info = info;
    if (message != NULL)
    {
        result->message = strdup(message);
    }
    else
    {
        result->message = NULL;
    }

    return result;
}

static void free_test_verifier_result(test_verifier_result_t* result)
{
    if (result->message != NULL)
    {
        free((void*)result->message);
    }

    free(result);
}

static test_result_t* make_test_result(const test_t* test)
{
    test_result_t* result = malloc(sizeof(test_result_t));
    memset(result, 0, sizeof(test_result_t));
    result->verifier_result_count = list_item_count(
        test->test_infos,
        has_verifier,
        NULL);
    result->verifier_results = malloc(result->verifier_result_count *
        sizeof(test_verifier_result_t*));
    memset(result->verifier_results, 0, sizeof(test_verifier_result_t*) *
        result->verifier_result_count);

    return result;
}

static void free_test_result(test_result_t* result)
{
    if (result == NULL)
    {
        return;
    }

    if (result->message != NULL)
    {
        free(result->message);
    }

    if (result->verifier_results != NULL)
    {
        for (int i = 0; i < result->verifier_result_count; i++)
        {
            if (result->verifier_results[i] != NULL)
            {
                free_test_verifier_result(result->verifier_results[i]);
            }
        }

        free(result->verifier_results);
    }

    free(result);
}

static test_t* init_test()
{
    test_t* test = malloc(sizeof(test_t));
    memset(test, 0, sizeof(test_t));
    return test;
}

static void finalize_test(test_t* test,
    const char* buffer,
    size_t source_start,
    size_t source_end)
{
    size_t bytecode_size = 0;
    test->source = strndup(buffer+source_start, source_end-source_start);
    test->bytecode = assemble(test->source, &bytecode_size);
    test->bytecode_size = bytecode_size;
}

static void free_test_info(test_info_t* info)
{
    free((char*)info->name);
    free(info);
}

static void free_test(test_t* test)
{
    test_info_t* info = test->test_infos;
    while(info != NULL)
    {
        test_info_t* next = (test_info_t*)list_next((intrusive_list_t*)info);
        free_test_info(info);
        info = next;
    }

    if (test->name != NULL)
    {
        free((char*)test->name);
    }
    if (test->source != NULL)
    {
        free((char*)test->source);
    }
    if (test->bytecode != NULL)
    {
        free((uint8_t*)test->bytecode);
    }

    if (test->last_result != NULL)
    {
        free_test_result(test->last_result);
    }
    free(test);
}

static test_verifier_result_t*  stack_verifier(test_info_t* info, void* data)
{
    cpu* cpu = (struct cpu_t*)data;
    bool status = false;
    char* message = NULL;
    if (strcmp("expected_byte", info->name) == 0)
    {
        // TODO: If sp is pointing to completely random places
        // this might SEGFAULT.
        status = GET_SP_B_VAL(cpu, -1) == (uint8_t)info->val;
        if (!status)
        {
            PRINT_ALLOCATE_BUFFER(message,  "expected byte: %" PRIu8
                ", got: %" PRIu8 "\n",  (uint8_t)info->val,
                GET_SP_B_VAL(cpu, -1));
        }
    }
    else if (strcmp("expected_word", info->name) == 0)
    {
        // TODO: If sp is pointing to completely random places
        // this might SEGFAULT.
        status = GET_SP_W_VAL(cpu, -2) == (uint16_t)info->val;
        if (!status)
        {
            PRINT_ALLOCATE_BUFFER(message, "expected word: %" PRIu16
                ", got: %" PRIu16 "\n",  (uint16_t)info->val, GET_SP_W_VAL(cpu, -2));
        }
    }

    test_verifier_result_t* result = make_test_verifier_result(
        status,
        info,
        message);
    free(message);
    return result;
}

static test_verifier_result_t*  flag_verifier(test_info_t* info, void* data)
{
    cpu* cpu = (struct cpu_t*)data;
    bool status = false;
    uint16_t expected_val = (uint16_t)info->val;
    char* message = NULL;

    if (strcmp("flag:zero", info->name) == 0)
    {
        status = flag_isset(cpu, FLAG_ZERO) == expected_val;
    }
    else if (strcmp("flag:run", info->name) == 0)
    {
        status = flag_isset(cpu, FLAG_RUN) == expected_val;
    }
    else if (strcmp("flag:sign", info->name) == 0)
    {
        status = flag_isset(cpu, FLAG_SIGN) == expected_val;
    }

    if (!status)
    {
        char* buf = print_cpu(cpu);
        PRINT_ALLOCATE_BUFFER(message, "cpu flag verifier %s failed. "
            "\nCPU: %s\n",
            info->name,
            buf);
        free(buf);
    }

    test_verifier_result_t* result = make_test_verifier_result(
        status,
        info,
        message);
    free(message);
    return result;
}

static test_verifier_result_t*  cpu_verifier(test_info_t* info, void* data)
{
    cpu* cpu = (struct cpu_t*)data;
    bool status = false;
    uint16_t expected_val = (uint16_t)info->val;
    char* message = NULL;

    if (strcmp("cpu:ip", info->name) == 0)
    {
        status = cpu->ip == expected_val;
    }
    else if (strcmp("cpu:sp", info->name) == 0)
    {
        status = cpu->sp == expected_val;
    }
    else if (strcmp("cpu:bp", info->name) == 0)
    {
        status = cpu->bp == expected_val;
    }

    if (!status)
    {
        char* buf = print_cpu(cpu);
        PRINT_ALLOCATE_BUFFER(message, "cpu_verifier for %s failed. "
            "CPU: %s\n",
            info->name,
            buf);
        free(buf);
    }

    test_verifier_result_t* result = make_test_verifier_result(
        status,
        info,
        message);
    free(message);
    return result;
}

static bool starts_with(const char* needle, const char* haystack)
{
    return strncmp(haystack, needle, strlen(needle)) == 0;
}

static test_verifier_result_t* (*get_verifier_for(const char* name))(
    test_info_t* info,
    void* data)
{
    if (starts_with("cpu:", name))
    {
        return cpu_verifier;
    }
    else if (starts_with("expected_", name))
    {
        return stack_verifier;
    }
    else if (starts_with("flag:", name))
    {
        return flag_verifier;
    }

    return NULL;
}

static test_info_t* make_test_info(const char* line)
{
    char name[32];
    const char* pos = line+1;
    if (!is_test_info(line))
    {
        return NULL;
    }

    pos = skip_whitespace(pos);
    uint8_t tpos = 0;
    while (!is_whitespace(pos) && !at_end(pos) && tpos < sizeof(name)-1)
    {
        name[tpos++] = *pos++;
    }
    name[tpos] = '\0';

    pos = skip_whitespace(pos);
    if (at_end(pos))
    {
        return NULL;
    }

    int32_t val = atoi(pos);
    test_info_t* info = malloc(sizeof(test_info_t));
    memset(info, 0, sizeof(info));
    info->name = strndup(name, 32);
    info->val = val;
    info->verifier = get_verifier_for(info->name);

    return info;
}

static test_result_t* verify(test_t* test, cpu* cpu)
{
    test_info_t* verifier = (test_info_t*)list_find_item(
        test->test_infos,
        has_verifier,
        NULL);
    test_result_t* test_result = make_test_result(test);
    if (test->last_result != NULL)
    {
        free_test_result(test->last_result);
    }
    test->last_result = test_result;

    bool status = true;
    int count = 0;
    while (verifier != NULL)
    {
        test_verifier_result_t* result = NULL;
        result = verifier->verifier(verifier, cpu);
        if (count < test_result->verifier_result_count)
        {
            test_result->verifier_results[count] = result;
            count++;
        }


        verifier = (test_info_t*)list_next((intrusive_list_t*)verifier);
        if (verifier != NULL)
        {
            verifier = (test_info_t*)list_find_item(
                    verifier,
                    has_verifier,
                    NULL);
        }
    }

    return test_result;
}

struct signal_info {
    cpu* cpu;
    test_t* test;
};

static struct signal_info* current_test;
static sigjmp_buf sb_env;
int last_signum;

void signal_handler(int signum, siginfo_t* info, void* data)
{
    flag_set(current_test->cpu, FLAG_RUN, 0);
    if (signum == SIGSEGV || signum == SIGTRAP)
    {
        last_signum = signum;
        siglongjmp(sb_env, 1);
    }
}

/****************/
/**** Extern ****/
/****************/


bool run_test(test_t* test)
{
    test_info_t* stack = (test_info_t*)list_find_item(
        test->test_infos,
        info_name_comparer,
        "max_stack");
    uint16_t stack_size = 32;
    if (stack != NULL)
    {
        stack_size = stack->val;
    }

    // Prepare stack for test cpu
    long page_size = sysconf(_SC_PAGESIZE);
    int pages_needed = stack_size / page_size + 2;

    uint8_t* stack_buffer = mmap(
        NULL,
        pages_needed*page_size,
        PROT_READ | PROT_WRITE,
        MAP_ANONYMOUS | MAP_PRIVATE,
        -1,
        0);
    if (stack_buffer == MAP_FAILED)
    {
        printf("Can't map stack for test cpu (%s)\n", strerror(errno));
        exit(-20);
    }
    memset(stack_buffer, STACK_INITIALIZATION_PATTERN, page_size*pages_needed);
    if (mprotect(stack_buffer+((pages_needed-1)* page_size), page_size, PROT_READ) != 0)
    {
        printf("Can't remove write permission from last stack page (%s)\n",
            strerror(errno));
        exit(-21);
    }


    // Create CPU and start the test
    bool status = true;
    uint8_t *stack_start = stack_buffer + (pages_needed-1)*page_size - stack_size;
    cpu* cpu = init_cpu_with_stack(stack_start);
    struct signal_info current_test_info = { cpu, test };
    current_test = &current_test_info;

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_sigaction = signal_handler;
    action.sa_flags = SA_SIGINFO | SA_RESETHAND;
    struct sigaction old_segv;
    struct sigaction old_trap;
    char* result_message = NULL;

    // Dangerzone!
    // Setup SEGV action handler and run the test
    // SEGV will indicate that the stack was pushed above the max_stack
    // limit. As the page after the specified max_stack is not readable/writable
    sigaction(SIGSEGV, &action, &old_segv);
    sigaction(SIGTRAP, &action, &old_trap);
    if (sigsetjmp(sb_env, 1))
    {
        status = false;
        switch (last_signum)
        {
        case SIGTRAP:
            result_message = strdup("BRK instruction encountered during test\n");
            break;
        case SIGSEGV:
            result_message = strdup("Max stack exceeded or invalid memory access\n");
            break;
        default:
            result_message = strdup("Internal error. Don't trust any of the results\n");
            break;
        }
    }
    else
    {
        run(cpu, test->bytecode);
    }
    sigaction(SIGSEGV, &old_segv, NULL);
    sigaction(SIGTRAP, &old_trap, NULL);

    // Verify the results
    test_result_t* result = verify(test, cpu);
    free_cpu(cpu);
    for (uint8_t *pos = stack_start-1; pos >= stack_buffer; pos--)
    {
        if (*pos != STACK_INITIALIZATION_PATTERN)
        {
            status = false;
            if (result_message != NULL)
            {
                char* temp = result_message;
                PRINT_ALLOCATE_BUFFER(result_message,
                    "%sArea above the stack was written.\n",
                    temp);
                free(temp);
            }
            else
            {
                result_message = strdup("Area above the stack was written.\n");
            }

            break;
        }
    }

    for (int i = 0; i < result->verifier_result_count; i++)
    {
        if (result->verifier_results[i]->status != 1)
        {
            status = false;
        }
    }
    result->message = result_message;
    munmap(stack_buffer, pages_needed*page_size);

    return status;
}

bool run_tests(test_t* tests)
{
    bool status = true;
    test_t* cur = tests;
    while (cur != NULL)
    {
        if (!run_test(cur))
        {
            status = false;
        }

        cur = (test_t*)list_next((intrusive_list_t*)cur);
    }

    return status;
}

test_t* create_tests(const char* buffer)
{
    size_t buffer_len = strlen(buffer);
    size_t buffer_pos = 0;
    char line[MAX_LINE_LENGTH];
    test_t* tests = NULL;
    test_t* current = NULL;
    bool in_source = false;
    size_t source_start = 0;
    size_t source_end = 0;

    while(*(next(buffer, line, &buffer_pos)) != '\0')
    {
        if (is_test_start(line))
        {
            if (source_start != 0)
            {
                finalize_test(current, buffer, source_start, source_end);
            }

            test_t* new;
            new = init_test();
            if (tests != NULL)
            {
                list_append(tests, (intrusive_list_t*)new);
            }
            else
            {
                tests = new;
            }

            new->name = copy_test_name(line);
            in_source = false;
            current = new;
            source_start = buffer_pos;
            source_end = 0;
        }
        else if (is_test_info(line) && !in_source)
        {
            source_start = buffer_pos;
            test_info_t* info = make_test_info(line);
            if (current->test_infos == NULL)
            {
                current->test_infos = info;
            }
            else
            {
                list_append(current->test_infos, (intrusive_list_t*)info);
            }
        }
        else if (!in_source)
        {
            in_source = true;
            source_end = buffer_pos;
        }
        else if (in_source)
        {
            source_end = buffer_pos;
        }
    }

    if (source_end != 0)
    {
        finalize_test(current, buffer, source_start, source_end);
    }

    return tests;
}

void free_tests(test_t* tests)
{
    while(tests != NULL)
    {
        test_t* next = (test_t*)list_next((intrusive_list_t*)tests);
        free_test(tests);
        tests = next;
    }
}
