#ifndef __TEST_H__
#define __TEST_H__

#include "ds/intrusive_list.h"
#include "cpu.h"

typedef struct test_verifier_result_t {
    int status;
    const struct test_info_t* info;
    const char* message;
} test_verifier_result_t;

typedef struct test_result_t {
    const struct test_t* test;
    int verifier_result_count;
    char* message;
    test_verifier_result_t** verifier_results;
} test_result_t;

typedef struct test_info_t {
    intrusive_list_t list;
    const char* name;
    int32_t val;
    test_verifier_result_t* (*verifier)(struct test_info_t* info, void* data);
} test_info_t;

typedef struct test_list_t {
    intrusive_list_t list;
    const char* name;
    const char* source;
    const uint8_t* bytecode;
    uint16_t bytecode_size;
    uint8_t test_info_count;
    test_info_t* test_infos;
    test_result_t* last_result;
} test_t;

extern test_t* create_tests(const char* buffer);
extern void free_tests(test_t* tests);

extern bool run_test(test_t* test);
extern bool run_tests(test_t* tests);

#endif
