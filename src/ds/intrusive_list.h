#ifndef __INTRUSIVE_LIST_H__
#define __INTRUSIVE_LIST_H__

#include "common.h"

typedef struct intrusive_list_t {
    void* next;
} intrusive_list_t;

extern intrusive_list_t* list_next(intrusive_list_t* item);
extern void list_append(void* list, intrusive_list_t* item);
extern intrusive_list_t* list_remove(void* list,
                                       intrusive_list_t* item);
extern intrusive_list_t* list_find_item(const void* list,
             bool (*comparer)(const intrusive_list_t* item, const void* criteria),
             const void* criteria); 

extern size_t list_item_count(const void* list,
             bool (*comparer)(const intrusive_list_t* item, const void* criteria),
             const void* criteria); 

#endif
