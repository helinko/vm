#include "../common.h"
#include "intrusive_list.h"

inline intrusive_list_t* list_next(intrusive_list_t* item)
{
    return item->next;
}

void list_append(void *list, intrusive_list_t* item)
{
    intrusive_list_t* cur = list;
    while (cur->next != NULL)
    {
        cur = list_next(cur);
    }

    cur->next = item;
    item->next = NULL;
}

intrusive_list_t* list_remove(void* list,
                                intrusive_list_t* item)
{
    intrusive_list_t* cur = list;
    if (cur == item)
    {
        return cur->next;
    }

    while (cur != NULL && cur->next != item)
    {
        cur = list_next(cur);
    }

    if (cur->next != NULL)
    {
        cur->next = list_next(cur->next);
    }

    return list;
}

intrusive_list_t* list_find_item(const void* list,
    bool (*comparer)(const intrusive_list_t* item, const void* criteria),
    const void* criteria)
{
    intrusive_list_t* cur = (intrusive_list_t*)list;
    while (cur != NULL)
    {
        if(comparer(cur, criteria) == true)
        {
            return cur;
        }

        cur = list_next(cur);
    }

    return NULL;
}

size_t list_item_count(const void* list,
             bool (*comparer)(const intrusive_list_t* item, const void* criteria),
             const void* criteria)
{
    size_t count = 0;
    intrusive_list_t* cur = (intrusive_list_t*)list;
    while (cur != NULL)
    {
        if (comparer != NULL)
        {
            if (comparer(cur, criteria) == true)
            {
                count++;
            }
        }
        else
        {
            count++;
        }

        cur = cur->next;
    }

    return count;
}
