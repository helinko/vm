#ifndef __ASSEMBLER_H__
#define __ASSEMBLER_H__
#include <inttypes.h>
#include <stddef.h>

#define MAX_TOKEN_LENGTH 32
#define INITIAL_CODE_SEGMENT_SIZE 512

extern uint8_t* assemble(const char* source, size_t* size);

#endif
