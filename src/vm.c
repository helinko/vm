#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "assembler.h"
#include "filesystem.h"
#include "cpu.h"

int main(int argc, char** argv)
{
    cpu* cpu = init_cpu(1024);
    char* source = NULL;
    if (argc < 2)
    {
        printf("No source to execute given. Defaulting to ./source.\n");
        source = read_file("./source");
    }
    else
    {
        source = read_file(argv[1]);
    }

    if (source == NULL)
    {
        printf("No source found nor file to execute given.\n");
        return -1;
    }

    size_t assembled_size = 0;

    uint8_t* assembled_code = assemble(source, &assembled_size);
    printf("assembled %lu bytes from source size of %lu\n",
        assembled_size,
        strlen(source));
    free(source);

    struct cpu_t* dcpu = init_cpu(1024);
    char* disassembled_code = disassemble(dcpu, assembled_code, assembled_size);
    printf("\n==== Disassembly ====\n%s\n", disassembled_code);
    free(disassembled_code);
    free_cpu(dcpu);
    dcpu = NULL;

    printf("\n==== Starting execution of assembled code ====\n");

    run(cpu, assembled_code);

    printf("\n==== Execution done ====\n");
    free(assembled_code);
    free_cpu(cpu);

	return 0;
}
