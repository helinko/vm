#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__
#include "common.h"

extern char* read_file(const char* path);

#endif
