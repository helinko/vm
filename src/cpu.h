#ifndef __CPU_H__
#define __CPU_H__

#include <stdint.h>
#include <stdio.h>

#define GET_IP_ADDR(cpu) (((cpu)->code)+((cpu)->ip))
#define GET_SP_ADDR(cpu) (((cpu)->data)+((cpu)->sp))
#define GET_BP_ADDR(cpu) (((cpu)->data)+((cpu)->bp))

#define GET_IP_B_VAL(cpu, offset) (*((uint8_t*)(GET_IP_ADDR((cpu)))+(offset)))
#define GET_SP_B_VAL(cpu, offset) (*((uint8_t*)(GET_SP_ADDR((cpu)))+(offset)))
#define GET_BP_B_VAL(cpu, offset) (*((uint8_t*)(GET_BP_ADDR((cpu)))+(offset)))
#define GET_IP_SB_VAL(cpu, offset) (*((int8_t*)(GET_IP_ADDR((cpu)))+(offset)))
#define GET_SP_SB_VAL(cpu, offset) (*((int8_t*)(GET_SP_ADDR((cpu)))+(offset)))
#define GET_BP_SB_VAL(cpu, offset) (*((int8_t*)(GET_BP_ADDR((cpu)))+(offset)))

#define GET_IP_W_VAL(cpu, offset) (*(uint16_t*)(((((uint8_t*)GET_IP_ADDR((cpu)))+(offset)))))
#define GET_SP_W_VAL(cpu, offset) (*(uint16_t*)(((((uint8_t*)GET_SP_ADDR((cpu)))+(offset)))))
#define GET_BP_W_VAL(cpu, offset) (*(uint16_t*)(((((uint8_t*)GET_BP_ADDR((cpu)))+(offset)))))

typedef struct cpu_t
{
    uint16_t ip;
    uint16_t sp;
    uint16_t bp;
    uint8_t flags;
    const uint8_t* code;
    uint8_t* data;
    uint8_t control_flags;
} cpu;

typedef enum
{
    FLAG_RUN,
    FLAG_ZERO,
    FLAG_SIGN
} CPU_FLAGS;

typedef enum
{
    CONTROL_FLAG_USER_STACK,
} CPU_CONTROL_FLAGS;

extern cpu* init_cpu(uint16_t stack_size);
extern cpu* init_cpu_with_stack(uint8_t* stack);
extern void free_cpu(cpu* cpu);
extern void run(cpu* cpu, const uint8_t* code);
extern char* disassemble(cpu* cpu, const uint8_t* code, const uint16_t size);
extern char* print_cpu(cpu* cpu);

inline void stack_push(cpu* cpu, uint8_t byte)
{
    *(GET_SP_ADDR(cpu)) = byte;
    cpu->sp++;
}

inline void stack_push_16(cpu* cpu, uint16_t val)
{
    *(GET_SP_ADDR(cpu)) = (uint8_t)(val & 0xff);
    cpu->sp++;
    *(GET_SP_ADDR(cpu)) = (uint8_t)((val >> 8) & 0xff);
    cpu->sp++;
}

inline uint8_t stack_pop(cpu* cpu)
{
    cpu->sp--;
    return GET_SP_B_VAL(cpu, 0);
}

inline uint16_t stack_pop_16(cpu* cpu)
{
    cpu->sp--;
    cpu->sp--;
    uint16_t val = GET_SP_W_VAL(cpu, 0);
    return val;
}

inline uint8_t fetch(cpu* cpu)
{
    uint8_t inst = GET_IP_B_VAL(cpu, 0);
    cpu->ip++;
    return inst;
}

inline uint16_t fetch_16(cpu* cpu)
{
    uint16_t inst = GET_IP_W_VAL(cpu, 0);
    cpu->ip++;
    cpu->ip++;
    return inst;
}

inline void flag_set(cpu* cpu, CPU_FLAGS flag, uint8_t val)
{
    cpu->flags ^= (-!!val ^ cpu->flags) & (1UL << flag);
}

inline int flag_isset(cpu* cpu, CPU_FLAGS flag)
{
    return (cpu->flags >> flag) & 1UL;
}

inline void control_flag_set(cpu* cpu, CPU_CONTROL_FLAGS flag, uint8_t val)
{
    cpu->control_flags ^= (-!!val ^ cpu->control_flags) & (1UL << flag);
}

inline int control_flag_isset(cpu* cpu, CPU_CONTROL_FLAGS flag)
{
    return (cpu->control_flags >> flag) & 1UL;
}


#endif
