import os.path;

def create_opcode(op):
    opcode_file = 'impl/' + op.lower() + ".c"
    if not os.path.isfile(opcode_file):
        with open(opcode_file, 'w+') as op_file:
            op_file.write("#include <signal.h>\n#include \"../../common.h\"\n")
            op_file.write("#include \"../../cpu.h\"\n\n")
            op_file.write("void fn_" + op.lower() + "(cpu* cpu)")
            op_file.write("{ raise(SIGTRAP); }")
    if os.path.isfile(opcode_file):
        skip = False
        with open(opcode_file, 'r') as check:
            if any("uint8_t dis_" + op.lower() + "(" in s for s in check.readlines()):
                skip = True
        if not skip:
            with open(opcode_file, 'a') as disassemble_file:
                disassemble_file.write("\n\nuint8_t dis_" + op.lower() +
                    "(cpu* cpu, char* buf, uint8_t bufsize)")
                disassemble_file.write("{ return 0; }\n")
    


if __name__ == "__main__":
    opcodes = []
    with open('opcodes.lst') as file:
        for op in file:
            if len(op.rstrip()) > 0:
                op_data = [x.rstrip() for x in op.split(" ") if len(x.rstrip()) > 0]
                opcodes.append(op_data)
                create_opcode(op_data[0])

    opcode_attributes = set([ attr for attrs in opcodes for attr in attrs[1:]])

    with open('opcodes.h', 'w+') as op_file:
        op_file.write("#ifndef __OPCODES_H__\n#define __OPCODES_H__\n")
        op_file.write("#include \"../cpu.h\"\n")
        
        op_file.write("\n#define NUM_OPCODES %d\n\n" % len(opcodes))
        op_file.write("\ntypedef enum\n{\n")
        for op in opcodes:
            op_file.write("\t" + op[0] + ",\n")

        op_file.write("} OP_CODES;\n\n");
        
        op_file.write("\ntypedef enum\n{\n")
        for attribute in opcode_attributes:
            op_file.write("\tATTR_" + attribute.upper() + ",\n")

        op_file.write("} OP_CODE_ATTRIBUTE;\n\n");

        op_file.write("typedef struct attr_descriptor_t {\n")
        op_file.write("\tint attribute_count;\n")
        op_file.write("\tconst OP_CODE_ATTRIBUTE* attributes;\n")
        op_file.write("} attr_descriptor_t;\n\n")

        for op in opcodes:
            op_file.write("extern void fn_" + op[0].lower() + "(cpu*);\n")
            op_file.write("extern uint8_t dis_" + op[0].lower()
                + "(cpu*, char*, uint8_t);\n")

        
        op_file.write("\nextern const char* op_code_names[];\n");
        op_file.write("\nextern const attr_descriptor_t op_code_attributes[];\n");
        op_file.write("\nextern void (*execute_op[])(cpu* cpu);");
        op_file.write("\nextern uint8_t (*disassemble_op[])(cpu* cpu, char* buf, uint8_t bufsize);");
        op_file.write("\n#endif\n")
        
    with open('opcodes.c', 'w+') as op_file:
        op_file.write("#include \"opcodes.h\"\n\nvoid (*execute_op[])(cpu* cpu) = {\n")
        for op in opcodes:
            op_file.write("\t fn_" + op[0].lower() + ",\n")
        op_file.write("};")
        op_file.write("\n\nuint8_t (*disassemble_op[])(cpu* cpu, char* buf, uint8_t bufsize) = {\n")
        for op in opcodes:
            op_file.write("\t dis_" + op[0].lower() + ",\n")
        op_file.write("};\n")
        
        op_file.write("\nconst char* op_code_names[] = {\n")
        for op in opcodes:
            op_file.write("\t\"" + op[0].lower()[3:] + "\",\n")
        op_file.write("};\n")
        
        attr_combinations = {}
        for op in opcodes:
            attr = op[1:]
            attr.sort()
            attr_combinations[".".join(attr)] = op[1:]

        attr_index = []
        index = 0
        for item in attr_combinations:
            op_file.write("\nconst OP_CODE_ATTRIBUTE __attr_t%d[] = { %s };" 
                % (index, ", ".join("ATTR_" + x.upper() for x in attr_combinations[item])))
            attr_index.append(item)
            index += 1

        op_file.write("\n\nconst attr_descriptor_t op_code_attributes[] = {\n")
        for op in opcodes:
            attrs = op[1:]
            attrs.sort()
            index = attr_index.index(".".join(attrs))
            op_file.write(("\t{ %d, __attr_t%d },\n" % (len(op[1:]), index)))
        op_file.write("};\n\n")




