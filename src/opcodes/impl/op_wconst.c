#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wconst(cpu* cpu)
{
    stack_push_16(cpu, fetch_16(cpu));
}

uint8_t dis_op_wconst(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WCONST %" PRIu16, GET_IP_W_VAL(cpu, 1));
    return 3;
}
