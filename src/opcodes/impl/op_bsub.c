#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_bsub(cpu* cpu)
{
    uint8_t b = stack_pop(cpu);
    uint8_t a = stack_pop(cpu);
    uint8_t r = a - b;
    stack_push(cpu, r);
    flag_set(cpu, FLAG_ZERO, r == 0);
    flag_set(cpu, FLAG_SIGN, (r >> 7) & 1);
}

uint8_t dis_op_bsub(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BSUB");
    return 1;
}
