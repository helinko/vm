#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_pop(cpu* cpu)
{
    uint8_t a = fetch(cpu);
    while(a-- > 0)
    {
        stack_pop(cpu);
    }
}

uint8_t dis_op_pop(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "POP %" PRIu8, GET_IP_B_VAL(cpu, 1));
    return 2;
}
