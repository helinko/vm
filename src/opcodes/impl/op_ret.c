#include <signal.h>
#include <stdio.h>
#include <inttypes.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_ret(cpu* cpu)
{
    uint8_t a = fetch(cpu);
    while(a-- > 0)
    {
        stack_pop(cpu);
    }
    
    cpu->bp = stack_pop_16(cpu);
    cpu->ip = stack_pop_16(cpu);
#ifdef _TRACE
    if (cpu->bp > cpu->sp)
    {
        printf("CPU->BP > CPU->SP %d > %d\n", cpu->sp, cpu->sp);
        raise(SIGTRAP);
    }
#endif
}

uint8_t dis_op_ret(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "RET %" PRIu8, *((uint8_t*)(GET_IP_ADDR(cpu)+1)));
    return 2;
}
