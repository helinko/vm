#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wsub(cpu* cpu)
{
    uint16_t b = stack_pop_16(cpu);
    uint16_t a = stack_pop_16(cpu);
    uint16_t r = a - b;
    stack_push_16(cpu, r);
    flag_set(cpu, FLAG_ZERO, r == 0);
    flag_set(cpu, FLAG_SIGN, (r >> 15) & 1);

}

uint8_t dis_op_wsub(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WSUB");
    return 1; 
}
