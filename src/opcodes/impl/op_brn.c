#include <inttypes.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_brn(cpu* cpu)
{
    int8_t offset = fetch(cpu);
    cpu->ip += offset;
}

uint8_t dis_op_brn(cpu* cpu, char* buf, uint8_t bufsize)
{  
    int8_t offset = *((int8_t*)(GET_IP_ADDR(cpu)+1));
    snprintf(buf, bufsize, "BRN %c0x%02X",
        offset < 0 ? '-' : '+',
        offset < 0 ? -offset : offset);
    return 2; 
}
