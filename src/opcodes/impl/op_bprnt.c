#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_bprnt(cpu* cpu)
{
    uint8_t a = stack_pop(cpu);
    printf("\n%d\n", a);
}


uint8_t dis_op_bprnt(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BPRNT");
    return 1;
}
