#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wprnt(cpu* cpu)
{
    uint16_t a = stack_pop_16(cpu);
    printf("\n%d\n", a);
}

uint8_t dis_op_wprnt(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WPRNT");
    return 1;
}
