#include <inttypes.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_brlv(cpu* cpu)
{
    uint8_t index = fetch(cpu);
    uint8_t val = *(GET_BP_ADDR(cpu)+index);
    stack_push(cpu, val);
}

uint8_t dis_op_brlv(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BRLV %" PRIu8, *(GET_IP_ADDR(cpu)+1));
    return 2;
}
