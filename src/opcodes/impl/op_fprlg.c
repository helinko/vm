#include <inttypes.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_fprlg(cpu* cpu)
{
    uint8_t a = fetch(cpu);
    stack_push_16(cpu, cpu->bp);
    cpu->bp = cpu->sp;
    while(a-- > 0)
    {
        stack_push(cpu, 0);
    }
}

uint8_t dis_op_fprlg(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "FPRLG %" PRIu8, *((uint8_t*)(GET_IP_ADDR(cpu)+1)));
    return 2;
}
