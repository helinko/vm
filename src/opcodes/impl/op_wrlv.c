#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wrlv(cpu* cpu)
{
    uint8_t index = fetch(cpu);
    uint16_t val = GET_BP_W_VAL(cpu, index);
    stack_push_16(cpu, val);
}

uint8_t dis_op_wrlv(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WRLV %" PRIu16, GET_IP_B_VAL(cpu, 1));
    return 2;
}
