#include <inttypes.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_call(cpu* cpu)
{ 
    uint8_t a = fetch(cpu);
    uint8_t b = fetch(cpu);
    stack_push_16(cpu, cpu->ip);
    cpu->ip = a << 8 | b;
}

uint8_t dis_op_call(cpu* cpu, char* buf, uint8_t bufsize)
{
    uint16_t location = *((uint8_t*)GET_IP_ADDR(cpu)+1) << 8 |
        *((uint8_t*)GET_IP_ADDR(cpu)+2);
    snprintf(buf, bufsize, "CALL 0x%04X", location);
    return 3; 
}
