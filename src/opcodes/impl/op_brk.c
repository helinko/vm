#include <signal.h>
#include <inttypes.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_brk(cpu* cpu)
{
    printf("\n====\nBRK at %04X\n====\n", cpu->ip-1);
    raise(SIGTRAP); 
}

uint8_t dis_op_brk(cpu* cpu, char* buf, uint8_t bufsize)
{ 
    snprintf(buf, bufsize, "BRK");
    return 1; 
}
