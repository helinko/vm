#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_bdup(cpu* cpu)
{
    uint8_t a = stack_pop(cpu);
    stack_push(cpu, a);
    stack_push(cpu, a);
}

uint8_t dis_op_bdup(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BDUP");
    return 1;
}
