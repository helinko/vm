#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wadd(cpu* cpu)
{
    uint16_t b = stack_pop_16(cpu);
    uint16_t a = stack_pop_16(cpu);
    stack_push_16(cpu, a + b);
}

uint8_t dis_op_wadd(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WADD");
    return 1;
}
