#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_bconst(cpu* cpu)
{
    stack_push(cpu, fetch(cpu));
}


uint8_t dis_op_bconst(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BCONST %" PRIu8, *(GET_IP_ADDR(cpu)+1));
    return 2;
}
