#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wrav(cpu* cpu)
{
    uint8_t var = fetch(cpu);
    uint16_t val = GET_BP_W_VAL(cpu, -(var+5));
    stack_push_16(cpu, val);
}

uint8_t dis_op_wrav(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WRAV %" PRIu8, GET_IP_B_VAL(cpu, 1));
    return 2;
}
