#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_bcmp(cpu* cpu)
{
    uint8_t b = stack_pop(cpu);
    uint8_t a = stack_pop(cpu);
    int8_t r = a-b;
    flag_set(cpu, FLAG_ZERO, r == 0);
    flag_set(cpu, FLAG_SIGN, (r >> 7) & 1);
}

uint8_t dis_op_bcmp(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BCMP");
    return 1;
}
