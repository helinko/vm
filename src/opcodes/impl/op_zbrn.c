#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_zbrn(cpu* cpu)
{
    int8_t offset = fetch(cpu);
    if (flag_isset(cpu, FLAG_ZERO))
    {
        cpu->ip += offset;
    }
}

uint8_t dis_op_zbrn(cpu* cpu, char* buf, uint8_t bufsize)
{ 
    snprintf(buf, bufsize, "ZBRN %" PRIi8, *((int8_t*)(GET_IP_ADDR(cpu)+1)));
    return 2;
}
