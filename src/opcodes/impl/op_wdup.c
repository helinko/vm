#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wdup(cpu* cpu)
{
    uint16_t w = stack_pop_16(cpu);
    stack_push_16(cpu, w);
    stack_push_16(cpu, w);
}

uint8_t dis_op_wdup(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "WDUP");
    return 1;
}
