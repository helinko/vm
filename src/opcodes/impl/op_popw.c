#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_popw(cpu* cpu)
{
    uint8_t a = fetch(cpu);
    while(a-- > 0)
    {
        stack_pop_16(cpu);
    }
}

uint8_t dis_op_popw(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "POPW %" PRIu8, GET_IP_B_VAL(cpu, 1));
    return 2; 
}
