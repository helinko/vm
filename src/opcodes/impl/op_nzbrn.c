#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_nzbrn(cpu* cpu)
{
    int8_t offset = fetch(cpu);
    if (!flag_isset(cpu, FLAG_ZERO))
    {
        cpu->ip += offset;
    }
}

uint8_t dis_op_nzbrn(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "NZBRN %" PRIi8, GET_IP_SB_VAL(cpu, 1));
    return 2;
}
