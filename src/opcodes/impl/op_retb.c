#include <signal.h>
#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_retb(cpu* cpu)
{
    uint8_t retval = stack_pop(cpu);

    uint8_t a = fetch(cpu);
    while(a-- > 0)
    {
        stack_pop(cpu);
    }
    
    cpu->bp = stack_pop_16(cpu);
    cpu->ip = stack_pop_16(cpu);
    stack_push(cpu, retval);
#ifdef _TRACE
    if (cpu->bp > cpu->sp)
    {
        printf("\n==== TRACE ABORT ====\nCPU->BP > CPU->SP (%d > %d) @%04X\n==== "
            "TRACE ABORT ====\n\n", cpu->bp, cpu->sp, cpu->ip);
        raise(SIGTRAP);
    }
#endif
}

uint8_t dis_op_retb(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "RETB %" PRIu8, GET_IP_B_VAL(cpu, 1));
    return 2;
}
