#include <stdio.h>
#include <inttypes.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_brav(cpu* cpu)
{
    uint8_t var = fetch(cpu);
    uint8_t val = GET_BP_B_VAL(cpu, -(var+5));
    stack_push(cpu, val);
}

uint8_t dis_op_brav(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BRAV %" PRIu8, *((uint8_t*)GET_IP_ADDR(cpu)+1));
    return 2;
}
