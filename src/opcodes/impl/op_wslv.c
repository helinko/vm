#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_wslv(cpu* cpu)
{
    uint8_t index = fetch(cpu);
    *(GET_BP_ADDR(cpu)+index+1) = stack_pop(cpu);
    *(GET_BP_ADDR(cpu)+index) = stack_pop(cpu);
}

uint8_t dis_op_wslv(cpu* cpu, char* buf, uint8_t bufsize)
{ 
    snprintf(buf, bufsize, "WSLV %" PRIu8, GET_IP_B_VAL(cpu, 1));
    return 2;
}
