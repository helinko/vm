#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_hlt(cpu* cpu)
{ 
    flag_set(cpu, FLAG_RUN, 0);
}


uint8_t dis_op_hlt(cpu* cpu, char* buf, uint8_t bufsize)
{ 
    snprintf(buf, bufsize, "HLT");
    return 1; 
}
