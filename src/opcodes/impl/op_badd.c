#include <stdio.h>
#include "../../common.h"
#include "../../cpu.h"

void fn_op_badd(cpu* cpu)
{
    uint8_t b = stack_pop(cpu);
    uint8_t a = stack_pop(cpu);
    stack_push(cpu, a + b);
}


uint8_t dis_op_badd(cpu* cpu, char* buf, uint8_t bufsize)
{
    snprintf(buf, bufsize, "BADD");
    return 1;
}
